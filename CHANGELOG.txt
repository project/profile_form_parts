
Content Profile Form Parts x.x-x.x, xxxx-xx-xx
----------------------------------------------


Content Profile Form Parts 6.x-1.x, xxxx-xx-xx
----------------------------------------------
by liammcdermott: fixed problem where node body was blanked when not included
  in form.
by liammcdermott: fixed non-displaying menu settings and node body.


Content Profile Form Parts 6.x-1.0-beta2, 2010-06-05
----------------------------------------------------
by liammcdermott: fixed error with searching an unset form #pre_render by
  wrapping it in a !empty() check.
by liammcdermott: updated CHANGELOG.txt (trying to get it to conform to
  http://drupal.org/node/52287).
by liammcdermott: changed README.txt to include configuration instructions.


Content Profile Form Parts 6.x-1.0-beta1, 2010-05-25
----------------------------------------------------
Initial release.
