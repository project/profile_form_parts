
CONTENTS OF THIS FILE
---------------------

 - Introduction
 - Installation
 - Configuration
 - Limitations


INTRODUCTION
------------

The purpose of Content Profile Form Parts is to take long user profile forms
produced by the Content Profile module and split them into parts, these may
then be placed anywhere on a user's profile page.

Parts are put into Drupal blocks, making them easy to place.


INSTALLATION
------------

1. This module requires the Content Profile module
   (http://drupal.org/project/content_profile) ensure it is in your
   sites/SITENAME/modules directory before moving on to step two of the
   installation.

2. Copy this profile_form_parts/ directory to your sites/SITENAME/modules
   directory.

3. Enable the Content Profile Form Parts module under
   Administer > Site building > Modules.


CONFIGURATION
-------------

 - To configure the form parts to be displayed on user profiles, go to
   Administration > Content management > Content types and click 'Profile' (or
   if you have created a different content type for your user profiles select
   that instead), then 'Content Profile' followed by 'Form parts'.

 - Get started by clicking 'Add new block', this will create a block that
   contains profile form parts.

 - 'Link text' is both the title of the block on the blocks configuration page
   and the link the user clicks to show the profile form parts.

 - The table beneath 'Link text' shows the available form parts, checking the
   'Include' box includes it in the profile form parts that are shown when the
   user clicks the link text on their profile.

 - The order form parts are displayed in can be changed by clicking and holding
   the drag handles (four-way arrow symbols) on each table row.
 
 - Once the blocks are created they have to be enabled and positioned on the
   block configuration page, by either clicking the link on Form Parts
   configuration page or by going to Administration > Site building > Blocks.


LIMITATIONS
-----------

 - This module is not designed to work with Drupal core's profile.module.

 - We deal with form 'parts' not 'fields' there is an important difference:
   'fields' would mean integration with CCK and having another module
   dependency, so instead the module works off the top-level items in
   a form, hence the term 'parts'. In many ways this is simpler and has the
   added benefit that we do not have to rely on another, often changing, API.
