<?php

/**
 * @file
 * Theme the blocks form.
 *
 * Available variables:
 * - $form: Array of form elements representing the admin interface.
 * - $unsaved_text: If there are unsaved changes to the form this will contain
 *   some text warning the user.
 * - $unsaved: Form controls that complement the warning text in $unsaved_text.
 * - $unsaved_message_shown: TRUE if the $unsaved_text and $unsaved were being
 *   shown to the user when the form was submitted, FALSE otherwise.
 * - Sblock_listing: An array of blocks containing form parts.
 *
 * Each $block_listing contains:
 * - $block_listing->link_text: Text the user clicks to download the profile
 *   form.
 * - $block_listing->remove: Remove button that deletes the block.
 * - $block_listing->parts: Array of profile form parts for the user to
 *   choose from.
 *
 * @see template_preprocess_profile_form_parts_blocks()
 *
 * @ingroup themeable
 */
?>
<?php if ($unsaved_warning_flag): ?>
  <?php print $unsaved_warning_flag; ?>
<?php endif; ?>
<?php if ($unsaved && !empty($block_listing)): ?>
  <div class="messages warning unsaved<?php if ($unsaved_message_shown): ?> shown<?php endif; ?>"><p><?php print $unsaved_text; ?></p> <?php print $unsaved; ?></div>
<?php endif; ?>

<?php if (empty($block_listing)): ?>
  <div class="profile-form-parts-block-header"><p><?php print t("No profile form parts have been added yet, to add one click <em>Add new block</em>."); ?></p></div>
<?php endif; ?>

<?php foreach ($block_listing as $delta => $data): ?>
  <div class="profile-form-parts-block-header">
  <?php print $data->link_text; ?>
  <?php print $data->remove; ?>
  </div>

  <div class="profile-form-parts-block-parts">
  <?php print $data->parts; ?>
  </div>
<?php endforeach; ?>
