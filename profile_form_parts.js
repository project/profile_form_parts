
/**
 * @file
 * Click handler that downloads forms, Javascript and CSS.
 */

/**
 * Attaches AJAX form parts downloader to each Content Profile Form Parts link.
 */
Drupal.behaviors.profileFormParts = function() {
  var parts = Drupal.settings.profileFormParts;
  for (var id in parts) if (parts.hasOwnProperty(id)) {
    $(parts[id].selector).click(function () {
      // See also: misc/ahah.js
      $(this).after('<div class="ahah-progress ahah-progress-throbber"><div class="throbber">&nbsp;</div></div>');
      // Use the anchor's id to find the parts[id]. The anchor's id looks
      // like: profile_form_parts_3_link so we remove '_link' from the end to
      // get the id.
      $.get(parts[this.id.substring(0, this.id.length-5)].url, null, Drupal.profileFormParts);

      return false;
    });
  }
}

/**
 * Takes a response from profile_form_parts.module, inserts it into the page.
 */
Drupal.profileFormParts = function(response) {
  var result = Drupal.parseJson(response);
  var newContent = $('<div></div>').html(result.data);

  Drupal.freezeHeight();
  
  // Loop through existing Javascript and CSS, if it's not already included
  // in the page add it, otherwise ignore it. This facilitates loading
  // additional scripts and styles that were not present in the original page,
  // when a module adds a stylesheet via drupal_add_css() during the AJAX page
  // load for example.

  // External JS files.
  var len = result.js.external.length;
  for (var js_count = 0; js_count < len; js_count++) {
    if ($('script[src^=' + result.js.external[js_count] + ']').length == 0) {
      $('head').append('<script type="text/javascript" src="' + result.js.external[js_count] + '"></script>');
    }
  }
  // Inline JS.
  var len = result.js.inline.length;
  for (var jsCount = 0; jsCount < len; jsCount++) {
    // Drupal.settings (which is added inline) always needs to be
    // re-initialised after adding new scripts.
    $('head').append('<script type="text/javascript">' + result.js.inline[jsCount] + '</script>');
  }
  // CSS
  for (var media in result.css) if (result.css.hasOwnProperty(media)) {
    var len = result.css[media].length;
    for (var css_count = 0; css_count < len; css_count++) {
      if ($('style[type=text/css]:contains("' + result.css[media][css_count] + '")').length == 0) {
        $('head').append('<link type="text/css" rel="stylesheet" media="' + media + '" href="' + result.css[media][css_count] + '" />');
      }
    }
  }

  $(result.wrapper).empty().append(newContent);
  newContent.hide();
  newContent.fadeIn();

  // Attach all javascript behaviors to the new content, if it was successfully
  // added to the page.
  if (newContent.parents('html').length > 0) {
    Drupal.attachBehaviors(newContent);
  }

  // Themers may create this function when there's a need to react to a profile
  // form being shown.
  if (typeof Drupal.profileFormParts.prototype.callBack == 'function') {
    Drupal.profileFormParts.prototype.callBack(result);
  }

  Drupal.unfreezeHeight();
}
