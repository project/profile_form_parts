
/**
 * @form
 * Javascript for the profile form parts admin page.
 */

/**
 * Attaches the Content Profile Form Parts admin actions to form elements.
 */
Drupal.behaviors.profileFormPartsAdmin = function() {
  $('input.link-text').change(function () {
    if ($(this).val() !== '') {
      Drupal.profileFormPartsAdmin.unsavedFlagShow();
    }
  });
  $('input.form-checkbox').change(function () {
    if ($('.profile-form-parts-block-parts:has(#' + this.id + ')').prev('.profile-form-parts-block-header').find('.link-text').val() !== '') {
      Drupal.profileFormPartsAdmin.unsavedFlagShow();
    }
  });
}

/**
 * Show the form's unsaved flag.
 */
Drupal.profileFormPartsAdmin.unsavedFlagShow = function() {
  // This simply shows the unsaved message to the user.
  $('#profile-form-parts-admin-settings .messages').css('visibility', 'visible');
  // Provide an internal flag, that will show up in a FAPI $form_state, so PHP
  // code can react to the state of the unsaved flag.
  $('form#profile-form-parts-admin-settings #edit-unsaved-warning-flag').attr('value', 1);
}
